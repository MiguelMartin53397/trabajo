import numpy as np
from librerias.evaluator import evaluator
from librerias import preflop
from librerias import flop
from librerias import turn
from librerias import river


def barajar(baraja):
	m = 500
	NUM_CARTAS = baraja.shape[0]
	c = NUM_CARTAS - 1
	
	label = np.array([list((i,0)) for i in baraja])
	label[:,1] = np.random.randint(1,m,NUM_CARTAS)[:]
	
	for i in range(1,m):
		for item in range(NUM_CARTAS):
			if (2*i - 1) == int(label[item][1]):
				baraja[c] = label[item][0]
				c = c -1
		for item in reversed(range(NUM_CARTAS)):
			if 2*i == int(label[item][1]):
				baraja[c] = label[item][0]
				c = c - 1



def establecerGanador(r,JyM,partidasG):
	#Vemos quien es el ganador de la partida

	resultado = np.array([ 0, 0, 0, 0, 0, 0])
	resultado[0] = evaluator.evaluate_cards(JyM[0][0],JyM[0][1],JyM[6][0],JyM[6][1],JyM[6][2],JyM[6][3],JyM[6][4])
	resultado[1] = evaluator.evaluate_cards(JyM[1][0],JyM[1][1],JyM[6][0],JyM[6][1],JyM[6][2],JyM[6][3],JyM[6][4])
	resultado[2] = evaluator.evaluate_cards(JyM[2][0],JyM[2][1],JyM[6][0],JyM[6][1],JyM[6][2],JyM[6][3],JyM[6][4])
	resultado[3] = evaluator.evaluate_cards(JyM[3][0],JyM[3][1],JyM[6][0],JyM[6][1],JyM[6][2],JyM[6][3],JyM[6][4])
	resultado[4] = evaluator.evaluate_cards(JyM[4][0],JyM[4][1],JyM[6][0],JyM[6][1],JyM[6][2],JyM[6][3],JyM[6][4])
	resultado[5] = evaluator.evaluate_cards(JyM[5][0],JyM[5][1],JyM[6][0],JyM[6][1],JyM[6][2],JyM[6][3],JyM[6][4])
	ganador = min(resultado)

	if ganador == resultado[0]:
		if r == 1:
			partidasG[1] = partidasG[1] + 1
		else:
			partidasG[0] = partidasG[0] + 1
	elif ganador == resultado[1]: 	
		partidasG[2] = partidasG[2] + 1
		if r == 0:
			partidasG[7] = partidasG[7] + 1
	elif ganador == resultado[2]: 	
		partidasG[3] = partidasG[3] + 1
		if r == 0:
			partidasG[7] = partidasG[7] + 1	
	elif ganador == resultado[3]: 	
		partidasG[4] = partidasG[4] + 1
		if r == 0:
			partidasG[7] = partidasG[7] + 1
	elif ganador == resultado[4]: 	
		partidasG[5] = partidasG[5] + 1
		if r == 0:
			partidasG[7] = partidasG[7] + 1
	else:
		partidasG[6] = partidasG[6] + 1
		if r == 0:
			partidasG[7] = partidasG[7] + 1










#Inicializamos la baraja

baraja = np.array([ '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', 'Tc', 'Jc', 'Qc', 'Kc', 'Ac',
	    '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', 'Td', 'Jd', 'Qd', 'Kd', 'Ad',
	    '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', 'Th', 'Jh', 'Qh', 'Kh', 'Ah',
	    '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', 'Ts', 'Js', 'Qs', 'Ks', 'As' ])



#inicializamos las variables para determinar si ganamos o no

pJ = 0						#partidas jugadas

partidasG = [ 0, 0, 0, 0, 0, 0, 0, 0 ]
						#partidasG[0] ----> partidas ganadas
						#partidasG[1] ----> partidas que ibamos a ganar pero nos hemos retirado
						#partidasG[2] ----> partidas ganadas por el jugador 2
						#partidasG[3] ----> partidas ganadas por el jugador 3
						#partidasG[4] ----> partidas ganadas por el jugador 4
						#partidasG[5] ----> partidas ganadas por el jugador 5
						#partidasG[6] ----> partidas ganadas por el jugador 6
						#partidasG[7] ----> partidas perdidas



for i in range(10000):
	
	pJ = pJ + 1

	#Barajamos
	barajar(baraja)


	#Repartimos
	JyM = np.array([ [ baraja[0],baraja[1] ], 						#maquina
		[ baraja[2],baraja[3] ], 							#J1
		[ baraja[4],baraja[5] ],							#J2
		[ baraja[6],baraja[7] ],							#J3
		[ baraja[8],baraja[9] ],							#J4
		[ baraja[10],baraja[11] ],							#J5
		[ baraja[12], baraja[13], baraja[14], baraja[15], baraja[16] ] ])		#mesa
	nJ = 6


	#Evaluamos el preflop
	respuestaPreflop = preflop.preflop(2000,20,JyM[0][0][0],JyM[0][0][1],JyM[0][1][0],JyM[0][1][1])
	if respuestaPreflop == "R":
		establecerGanador(1,JyM,partidasG)
		continue


	#Evaluamos el flop
	respuestaFlop = flop.flop(2000, 20, nJ, JyM[0][0][0],JyM[0][0][1],JyM[0][1][0],JyM[0][1][1],JyM[6][0][0],JyM[6][0][1],JyM[6][1][0],JyM[6][1][1],JyM[6][2][0],JyM[6][2][1])
	if respuestaFlop == "R":
		establecerGanador(1,JyM,partidasG)
		continue


	#Evaluamos el turn
	respuestaTurn = turn.turn(2000, 20, nJ, JyM[0][0][0],JyM[0][0][1],JyM[0][1][0],JyM[0][1][1],JyM[6][0][0],JyM[6][0][1],JyM[6][1][0],JyM[6][1][1],JyM[6][2][0],JyM[6][2][1],JyM[6][3][0],JyM[6][3][1])
	if respuestaTurn == "R":
		establecerGanador(1,JyM,partidasG)
		continue



	#Evaluamos el river
	respuestaRiver = river.river(2000, 20, nJ, JyM[0][0][0],JyM[0][0][1],JyM[0][1][0],JyM[0][1][1],JyM[6][0][0],JyM[6][0][1],JyM[6][1][0],JyM[6][1][1],JyM[6][2][0],JyM[6][2][1],JyM[6][3][0],JyM[6][3][1],JyM[6][4][0],JyM[6][4][1])
	if respuestaRiver == "R":
		establecerGanador(1,JyM,partidasG)
		continue
	
	
	#Hora del showdown
	establecerGanador(0,JyM,partidasG)


	



print("partidas jugadas: ", pJ)
print("partidas ganadas: ", partidasG[0])
print("partidas ganadas habiendonos retirado: ", partidasG[1])
print("partidas ganadas jugador2: ", partidasG[2])
print("partidas ganadas jugador3: ", partidasG[3])
print("partidas ganadas jugador4: ", partidasG[4])
print("partidas ganadas jugador5: ", partidasG[5])
print("partidas ganadas jugador6: ", partidasG[6])
print("partidas perdidas: ", partidasG[7])










