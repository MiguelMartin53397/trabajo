import os
import sys
import math
from librerias import preflop
from librerias import flop
from librerias import turn
from librerias import river

def receiveGame():

    fifo = open("./FIFO", "r")            #abrimos el FIFO para leerlo
    datosJuego = fifo.read()                #leemos el FIFO
    fifo.close()                            #cerramos el FIFO
    return datosJuego

def sendFeedback(respuesta):
    os.mkfifo("FIFO_2")                     #creamos el segundo FIFO para mandar respuesta
    fifo = open("FIFO_2", "w")              #abrimos el FIFO
    fifo.write(respuesta)                   #escribirmos mensaje en el mecanismo de comunicacion
    
    fifo.close()                            #cerramos el FIFO
    os.remove("./FIFO_2")                   #borramos FIFO




sword = ("As", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K")
club = ("As", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K")
diamond = ("As", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K")
heart = ("As", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K")

datosJuego = receiveGame()

print("Hemos recibido el mensaje: ")
print(datosJuego)

#Aqui va el codigo del tratamiento de la variable datosJuego y el codigo de la IA 

respuesta = preflop.preflop(2000,20,1,"heart",1,"club")


sendFeedback(respuesta)

print("Hemos enviado el mensaje: ")
print(respuesta)

print("FIN DEL PROGRAMA")







