def comprobarProyectoColor(cartas):
	nC = 0
	nH = 0
	nD = 0
	nS = 0
	for i in range(len(cartas)):
		if cartas[i][1] == "c":
			nC += 1
		elif cartas[i][1] == "h":
			nH += 1
		elif cartas[i][1] == "d":
			nD += 1
		elif cartas[i][1] == "s":
			nS += 1
	if nC == 4 or nH == 4 or nD == 4 or nS == 4:
		return 1
	else:
		return 0
		
def comprobarProyectoEscalera(cartas):
	n = 0
	lista = []
	for i in range(len(cartas)):
		if cartas[i][0] == "A":
			lista.append(1)
			lista.append(14)
		elif cartas[i][0] == "2":
			lista.append(2)
		elif cartas[i][0] == "3":
			lista.append(3)
		elif cartas[i][0] == "4":
			lista.append(4)
		elif cartas[i][0] == "5":
			lista.append(5)
		elif cartas[i][0] == "6":
			lista.append(6)
		elif cartas[i][0] == "7":
			lista.append(7)
		elif cartas[i][0] == "8":
			lista.append(8)
		elif cartas[i][0] == "9":
			lista.append(9)
		elif cartas[i][0] == "T":
			lista.append(10)
		elif cartas[i][0] == "J":
			lista.append(11)
		elif cartas[i][0] == "Q":
			lista.append(12)
		else:
			lista.append(13)
	
	while True:
		aux = []
		for i in range(len(lista)-1):
			for j in range(i+1,len(lista)):
				if lista[i] == lista[j]:
					aux.append(lista[j])
				else:
					pass
		if len(aux) > 0:
			lista.remove(aux[len(aux)-1])
			aux.clear()
		else:
			break		
	lista.sort()
	lista.append(55)
	for i in range(len(lista)-1):
		if i == (len(lista)-2):
			break
		else:
			pass
		if (lista[i+1] == lista[i] + 1) or (lista[i+2] == lista[i] + 2):
			if lista[i+1] == lista[i] + 1:
				n += 1
				if n == 3:
					return 1
				else:
					continue
			else:
				n += 1
				if n == 3:
					return 1
				else:
					continue
		else:
			n = 0
	return 0
				
	



def potencialManoFlop(jugada,cartas):
	pC = comprobarProyectoColor(cartas)
	pE = comprobarProyectoEscalera(cartas)
	
	if jugada[0][0] == "HIGH_CARD":
		jugada[0][1] = "PAIR"
		jugada[0][2] = 15
	elif jugada[0][0] == "PAIR":
		jugada[0][1] = "TWO_PAIR"
		jugada[0][2] = 9
		jugada.append(["PAIR", "THREE_OF_A_KIND", 2])
	elif jugada[0][0] == "TWO_PAIR":
		jugada[0][1] = "FULL_HOUSE"
		jugada[0][2] = 4
	elif jugada[0][0] == "THREE_OF_A_KIND":
		jugada[0][1] = "FULL_HOUSE"
		jugada[0][2] = 6
		jugada.append(["THREE_OF_A_KIND", "FOUR_OF_A_KIND", 1])
	elif jugada[0][0] == "FULL_HOUSE":
		jugada[0][1] = "FOUR_OF_A_KIND"
		jugada[0][2] = 1
	else:
		pass
		
	if pE == 1:
		jugada.append( [jugada[0][0], "PROJ_STRAIGHT", 8] )
	else:
		pass
	
	if pC == 1:
		jugada.append( [jugada[0][0], "PROJ_FLUSH", 9] )
	else:
		pass


def potencialManoTurn(jugada,cartas):
	pC = comprobarProyectoColor(cartas)
	pE = comprobarProyectoEscalera(cartas,pC)
	
	if jugada[0][0] == "HIGH_CARD":
		jugada[0][1] = "PAIR"
		jugada[0][2] = 18
	elif jugada[0][0] == "PAIR":
		jugada[0][1] = "TWO_PAIR"
		jugada[0][2] = 12
		jugada.append(["PAIR", "THREE_OF_A_KIND", 2])
	elif jugada[0][0] == "TWO_PAIR":
		jugada[0][1] = "FULL_HOUSE"
		jugada[0][2] = 4
	elif jugada[0][0] == "THREE_OF_A_KIND":
		jugada[0][1] = "FULL_HOUSE"
		jugada[0][2] = 9
		jugada.append(["THREE_OF_A_KIND", "FOUR_OF_A_KIND", 1])
	elif jugada[0][0] == "FULL_HOUSE":
		jugada[0][1] = "FOUR_OF_A_KIND"
		jugada[0][2] = 1
	else:
		pass
		
	if pE == 1:
		jugada.append( [jugada[0][0], "PROJ_STRAIGHT", 8] )
	else:
		pass
		
	if pC == 1:
		jugada.append( [jugada[0][0], "PROJ_FLUSH", 9] )
	else:
		pass
		



def potencialMano(ronda,jugada,mano,tapete):
	cartas = mano + tapete
	if ronda == 2:
		potencialManoFlop(jugada,cartas)
	else:
		potencialManoTurn(jugada,cartas)

