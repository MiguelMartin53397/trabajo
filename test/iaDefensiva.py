from evaluation import evaluator
from evaluation import eval_card as eval_c
from potencialMano import potencialMano as ptj
from PreFlop import PreFlop as pf
from Flop import Flop as flop



#variables de accion
accion = [ 0, 0, 0, 0, 0 ]
Check = accion[0]
Call = accion[1]
Bet = accion[2]
Raise = accion[3]
Fold = accion[4]

#almacenamiento de la mano
jugada = [ [" ", " ", 0] ]

#valiables booleanas de farol
semiBluffing:bool = False
Bluffing:bool = False

#Ronda actual
ronda = 0                         #ronda = 1 Preflop  --  ronda = 2 Flop  --  ... 


###############################################################################
###############################################################################
###############################################################################
#######################Llegada de datos########################################


mano = [ "7c", "2c" ]

tapete = [ "3c", "9h", "Kd" ]

ronda = 2

apostar:bool = True

bote = 100

apuesta = 20

###############################################################################
###############################################################################









###############################################################################
###############################################################################
###############################################################################
###########################Llamada al bloque evaluador de manos################

#rango de la mano

if ronda == 2:								#Flop
	hr = evaluator.Evaluator.evaluate(evaluator.Evaluator(),[eval_c.EvaluationCard.new(mano[0]), eval_c.EvaluationCard.new(mano[1])],[eval_c.EvaluationCard.new(tapete[0]),eval_c.EvaluationCard.new(tapete[1]),eval_c.EvaluationCard.new(tapete[2])])
elif ronda == 3:							#Turn
	hr = evaluator.Evaluator.evaluate(evaluator.Evaluator(),[eval_c.EvaluationCard.new(mano[0]), eval_c.EvaluationCard.new(mano[1])],[eval_c.EvaluationCard.new(tapete[0]),eval_c.EvaluationCard.new(tapete[1]),eval_c.EvaluationCard.new(tapete[2]),eval_c.EvaluationCard.new(tapete[3])])
elif ronda == 4:							#River
	hr = evaluator.Evaluator.evaluate(evaluator.Evaluator(),[eval_c.EvaluationCard.new(mano[0]), eval_c.EvaluationCard.new(mano[1])],[eval_c.EvaluationCard.new(tapete[0]),eval_c.EvaluationCard.new(tapete[1]),eval_c.EvaluationCard.new(tapete[2]),eval_c.EvaluationCard.new(tapete[3]),eval_c.EvaluationCard.new(tapete[4])])
else:
	print("Error al configurar la ronda")


#tipo de mano (pareja, doble pareja, trio, ...) 
am = evaluator.Evaluator.get_rank_class(evaluator.Evaluator(),hr)

if am == 1:
	jugada[0][0] = "STRAIGHT_FLUSH"
elif am == 2:
	jugada[0][0] = "FOUR_OF_A_KIND"
elif am == 3:
	jugada[0][0] = "FULL_HOUSE"
elif am == 4:
	jugada[0][0] = "FLUSH"
elif am == 5:
	jugada[0][0] = "STRAIGHT"
elif am == 6:
	jugada[0][0] = "THREE_OF_A_KIND"
elif am == 7:
	jugada[0][0] = "TWO_PAIR"
elif am == 8:
	jugada[0][0] = "PAIR"
elif am == 9:
	jugada[0][0] = "HIGH_CARD"
else:
	print("No hemos identificado bien carta")
	

###############################################################################
###############################################################################









###############################################################################
###############################################################################
###############################################################################
######################Llamada al bloque potencial mano#########################

if ronda == 2:
	ptj.potencialMano(2,jugada, mano, tapete)
elif ronda == 3:
	ptj.potencialMano(3,jugada, mano, tapete)
else:
	pass

###############################################################################
###############################################################################










###############################################################################
###############################################################################
###############################################################################
#####################Llamada al bloque turno###################################

if ronda == 1:                                   		#Pre-FLop
	v = pf.PreFlop(mano,apostar,accion)
elif ronda == 2:				  		#Flop
	v = flop.Flop(accion,jugada,hr,apostar,apuesta,bote)
elif ronda == 3:					  	#Turn
	pass
elif ronda == 4:					  	#River
	pass
else:
	print("Truno equivocado")

###############################################################################
###############################################################################



print(accion)
print(jugada)
























