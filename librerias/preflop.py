


def preflop(promedioSaldo, apuesta, numero1, palo1, numero2, palo2):
    caso = apuesta / promedioSaldo * 100;
    
    if caso > 5:
        return casoApuestaInicialGrande(numero1, palo1, numero2, palo2);

    elif caso < 0.8:
        return casoApuestaInicialPeque();
    else:
        return casoNormal()




def casoNormal():
    return "V"

def casoApuestaInicialGrande(numero1, palo1, numero2, palo2):
    
    if numero1 == numero2:
        return "V"
    
    if palo1 == palo2 and (numero1 + numero2) > 16:
        return "V"
    else:
        return "R"


def casoApuestaInicialPeque():
    return "Vp"



