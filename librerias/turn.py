from librerias.evaluator import evaluator

def actuarBuenaMano(bote, apuesta):
	a = bote * (5/100)
	if apuesta == 0:
		return "a" + str(a)
	elif apuesta <= a:
		return "a" + str(a)
	else:
		return "v"
		
	
	

def actuarPosibilidadMano(bote, apuesta, numeroJugadores, numero1, palo1, numero2, palo2, num1, pal1, num2, pal2, num3, pal3, num4, pal4):
	if apuesta == 0:
		return "p"
	else:
		return "v"

def actuarMalaMano(apuesta):
	if apuesta == 0:
		return "p"
	else:
		return "R"


def turn(bote, apuesta, numeroJugadores, numero1, palo1, numero2, palo2, num1, pal1, num2, pal2, num3, pal3, num4, pal4):

    valor = evaluator.evaluate_cards(numero1+palo1,numero2+palo2,num1+pal1,num2+pal2,num3+pal3,num4+pal4)


    if valor >= 5305:
        return actuarMalaMano(apuesta)
    elif valor >= 3018 and valor < 5305:
        return actuarPosibilidadMano(bote, apuesta, numeroJugadores, numero1, palo1, numero2, palo2, num1, pal1, num2, pal2, num3, pal3, num4, pal4)
    else:
        return actuarBuenaMano(bote,apuesta)


